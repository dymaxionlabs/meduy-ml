# meduy

Paquete de Python del framework de aprendizaje automático y procesamiento de
datos desarrollado en el marco del proyecto Manos en la Data.

## Instalación

Las siguientes instrucciones están ajustadas para Linux, en particular la
distribución Ubuntu 20.04. El código desarrollado requiere Python 3.6+ y otras
bibliotecas para el procesamiento de imágenes raster y vectoriales, como GDAL,
OGR y Proj4.

Ejecutar la siguiente línea para instalar estas dependencias:

```bash
sudo apt-get install python3 python3-gdal gdal-bin
```

Clone este repositorio e ingrese en el directorio:

```bash
git clone git@github.com:AGESIC-UY/meduy-ml.git
cd meduy-ml/
```

Recomendamos crear un entorno virtual de Python, para instalar las dependencias
del proyecto localmente:

```bash
virtualenv -p python3 .venv/
pip install .
```

## Uso

Si al instalar creó un entorno virtual, debe activarlo primero:

```bash
cd meduy-ml/
source .venv/bin/activate
```

Ahora puede utilizar el paquete `meduy` desde python, por ejemplo:

```python
from meduy.unet.predict import PredictConfig, predict

cfg = PredictConfig(...)
predict(cfg)
```

## Licencia

Ver [LICENSE.txt](LICENSE.txt)
